package main

import "fmt"

func main() {
	n1, l1 := FullName("Piotr", "Koska")
	fmt.Printf("FullName: %v, number of charts: %v", n1, l1)
	fmt.Println(" ")

	n2, l2 := FullNameNakedReturn("Artur", "Kowalski")
	fmt.Printf("FullNameNakedReturn: %v, number of charts: %v", n2, l2)
	fmt.Println(" ")
}

func FullName(firstname, lastname string) (string, int) {
	full := firstname + " " + lastname
	lenght := len(full)
	return full, lenght
}

func FullNameNakedReturn(firstname, lastname string) (full string, lenght int) {
	full = firstname + " " + lastname
	lenght = len(full)
	return
}