package main

import (
	"fmt"
	"os"
	"errors"
)

func main() {
	f, err := os.Open("filename.txt")

	if err == nil {
		fmt.Println(f)
	} else {
		fmt.Println(err.Error())
	}

	myError := errors.New("My error string")
	fmt.Println(myError)

	a := map[string]bool{
		"Ann": true,
		"Mike": true}

	aa, ok := a["M"]
	if ok {
		fmt.Println("Mike attended?", aa)
	} else {
		fmt.Println("No info for Mike")
	}
}