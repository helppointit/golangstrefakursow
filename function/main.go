package main

import (
	"fmt"
	"./message"
)

func main() {
	m := message.Greeting("Piotr", "Witaj")
	fmt.Println(m)
}

//func greeting(name string, message string) string {
//	return fmt.Sprintf("%s - %s", message, name)
//}

//func greeting(name, message string) (salut string) {
//	salut = fmt.Sprintf("%s - %s", message, name)
//	return
//}

