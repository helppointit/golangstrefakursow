package main

import "fmt"

func main() {
	//names := [3]string{"Piotr", "Monika", "Milosz"}
	var names [3]string
	names[0] = "Piotr"
	names[1] = "Monika"
	//names[2] = "Milosz"
	//names[3] = "4 element"

	fmt.Println(names)
	fmt.Println("names[2] is nil: ", names[2] == "")
}