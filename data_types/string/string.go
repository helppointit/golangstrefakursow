package main

import "fmt"

func main() {
	fmt.Println("Simple String")
	fmt.Println(`
This is multi-line \n
String, that can also contain "quotes".
`)
	fmt.Println("\u2272")
	fmt.Println("Also in quotes I can use \n new line with \\n")
	fmt.Println('N')
}
