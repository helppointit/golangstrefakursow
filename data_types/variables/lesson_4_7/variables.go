package main

import "fmt"

func main() {
	var name string
	var val, _ = "yes", true

	myInt := 16

	name = "Piotr"

	fmt.Println("myInt is: ", myInt)
	fmt.Println("myInt times two: ", myInt * 2)
	fmt.Println("val is: ", val)
	fmt.Println("ok is: ", name)
}