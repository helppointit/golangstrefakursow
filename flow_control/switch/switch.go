package main

import "fmt"

func main() {
	floor := map[string]int{}
	floor["Winda1"] = 2

	switch floor["Winda1"] {
	case 1, 2, 3, 4:
		fmt.Println("Pietro zastrzezone")
	case 5:
		fmt.Println("Jedziemy na pientro 5")
	case 6:
		fmt.Println("Jedziemy na pientro 6")
	case 7:
		fmt.Println("Jedziemy na pientro 7")
	case 8, 9:
		fmt.Println("Pietro zastrzezone")
	default:
		fmt.Println("Nacisnij przycisk")
	}
}