package main

import (
	"fmt"
)

func main() {
	birthdays := map[string]string{
		"Piotr":"02/06/1990",
		"Monika":"01/01/1953",
		"Milosz":"04/10/1920",
	}

	ages := map[string]int{}
	ages["Piotr"] = 64
	ages["Monika"] = 22
	ages["Milosz"] = 11
	//ages["Piotr"] = 33

	delete(ages, "Monika")

	fmt.Println(ages, ages["Piotr"])

	fmt.Println(birthdays)
}