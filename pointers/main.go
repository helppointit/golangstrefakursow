package main

import "fmt"

func main() {
	var p *int
	var v int = 42
	p = &v
	if p !=nil {
		fmt.Println("Value of p:", *p)
		fmt.Println("Address of p:", p)
		fmt.Println("Address of p:", &v)
	} else {
		fmt.Println("p is nil")
	}

	var v1 float64 = 42.64
	p1 := &v1
	fmt.Println("v1 :", *p1)

	*p1 = *p1 / 31
	fmt.Println("v1 :", *p1)
	fmt.Println("v1 :", v1)
	
}