package main

import (
	"fmt"
)

func main() {
	fmt.Println("1. Greater than: ", 1 > 2)
	fmt.Println("2. Less than: ", 2 < 3)
	fmt.Println("3. Greater than OR equal: ", 2 >= 2)
	fmt.Println("4. Less than OR equal: ", 4 <= 4)
	fmt.Println("5. Equivalent: ", 4.1 == 4.0)
	fmt.Println("6. Not equivalent: ", 4.1 != nil)
}