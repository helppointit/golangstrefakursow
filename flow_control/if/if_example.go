package main

import "fmt"

func main() {
	points := map[string]int{}
	points["Piotr"] = 800

	if points["Piotr"] < 200 {
		fmt.Println("Brak znizki")
	} else if points["Piotr"] < 500 {
		fmt.Println("Znizka 10%")
	} else {
		fmt.Println("Znizka 15%")
	}
}