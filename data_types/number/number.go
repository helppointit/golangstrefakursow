package main

import (
	"fmt"
	"math"
)

func main() {
	//fmt.Println("Addition: ", 1+3)
	//fmt.Println("Subtraction: ", 27-19)
	fmt.Println("Multiplication: ", 9.0*11)
	fmt.Println("Division: ", 100/3)
	fmt.Println("Exponents: ", math.Pow(10, 2)/3)
}
