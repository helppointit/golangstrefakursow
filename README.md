# GoLangStrefaKursow  

<table align="center"><tr><td align="center" width="9999"><img src="https://pbs.twimg.com/profile_images/1108330399539830784/gSJjEUVx.png" align="center" width="550" alt="Project icon"></td></tr></table>

## Table of Content
1. **Wprowadzenie**
   1. Wstęp
   2. Jak korzystac z materiałów
   3. Przygotowanie środowiska instalacji VirtualBox
   4. Instalacja systemu CentOS
   5. Instalacja Ubuntu
   6. Dokumentacja i sekcja Download
2. **Przygotowanie środwiska**
   1. Instalacja GoLang w CentOS7
   2. Instalacja GoLang w Ubuntu
   3. Instalacja GoLang w Docker
   4. Instalacja w systemie Windows / WSL
   5. Instalacja i konfiguracja Visual Studio Code
3. **Uruchamianie Go**
   1. Tworzymy i uruchamiamy proste skrypty w GO
   2. Używamy Komentarzy
   3. Play Golang
4. **Typy danych**
   1. String i Charakter - co mówi nam dokumentacja
   2. String i Charakter - przykłady praktyczne
   3. Numbers - operatory podstawowe
   4. Numbers - integer, float - liczby całkowite i zmienno przecinkowe
   5. Numbers - wiecej operatorów matematycznych
   6. Booleans i 'nil'
   7. Praca ze zmiennymi - definiowanie zmiennych
   8. Syntax zmiennych
   9. Deklaracja zmiennej "Shorthand"
   10. Array
   11. Array i domyslne wartości
   12. Slices
   13. Maps
5. **Flow Control**
   1. If statement
   2. Else
   3. Else if
   4. Switch
   5.	Pętla for
   6.	Inne spojrzenie na pętle for
   7.	Pętla for - nieskończona i przerwana
6.	**Praca z Go**
   1.	Praca z funkcją
   2.	Praca z funcją i własny import
   3.	Czytanie input od użytkownika
   4.	Praca z pointers (wskaznikami)
   5.	Zwracanie wielu wartości z funkcji
   6.	Struct
   7.	Metoda
   8.	Interfejsy
   9.	Error
   10.	Zapis do pliku
   11.	Odczyt z pliku
7.	**Zakonczenie**
   1.	Zakonczenie

## Lista Kursów z którymi warto się zapoznać:

### Tematyka DevOps
ścieżka kariery zwawierając pakiet kursów który pomoże Ci w poznaniu zagadnien i tematu związanego z DevOps.
Ścieżka kariery [DevOps](https://strefakursow.pl/sciezki_kariery/devops_engineer.html?ref=52966)

#### **CI/CD**
Continuous integration (CI) i Continuous Delivery (CD) to zbiór zasad, wytycznych, kultura pracy i kolekcja dobrych praktyk dotyczących pracy nad projektami informatycznymi. Dzięki nim zespół developerów ma możliwość częstszego dostarczania pewnych, przetestowanych i sprawdzonych zmian w kodzie. Implementacja tych praktyk często nazywana jest CI/CD pipeline i jest uważana za jeden z najlepszych i najefektywniejszych sposobów pracy nad projektami informatycznymi i ich rozwojem.

1. Kurs wprowadzający do systemu wspomagający prace CI/CD jakim jest [Jenkins](https://videopoint.pl/kurs/jenkins-kurs-video-zostan-administratorem-systemow-it-piotr-koska,vpuppe.htm)
2. https://strefakursow.pl/kursy/it_i_oprogramowanie/kurs_jenkins_-_nowoczesny_workflow_ci_cd.html?ref=52966
https://videopoint.pl/kurs/openshift-kurs-video-zostan-administratorem-systemow-it-piotr-koska,vopskv.htm


#### **Automatyzacja**
Automatyzacja zadań czy konfiguracji przyśpiesza naszą pracę i czyni konfiguracje środwiska jednolitą.

1. Automatyzacja z wykorzystaniem [Ansible - Podstawy](https://videopoint.pl/kurs/ansible-kurs-video-automatyzacja-w-it-od-podstaw-piotr-koska,vansib.htm)
2. Ansible w zadaniach praktycznych - [Kurs Ansible](https://strefakursow.pl/kursy/programowanie/kurs_ansible_-_automatyzacja_zadan_w_praktyce.html?ref=52966)

#### **Cloud**
Syatemy cloud i platformy stają sie coraz popularniejsze i warto je poznać. Nauczysz sie z tych praktycznych kursów jak je wykorzystywać.  

1. Poznaj Platformę AWS [Amazon Web Services](https://videopoint.pl/kurs/amazon-web-services-aws-kurs-video-zostan-administratorem-systemow-it-piotr-koska,vawses.htm)

2. Poznaj platforma [Microsoft Azure](https://videopoint.pl/kurs/microsoft-azure-kurs-video-zostan-administratorem-systemow-it-piotr-tenyszyn,vmsazu.htm)

3. Kurs dotyczący [Microsoft Azure uczący podstaw tej platformy](https://strefakursow.pl/kursy/it_i_oprogramowanie/kurs_microsoft_azure_od_podstaw.html?ref=52966)

4. Poznaj platformę [Google Cloud](https://videopoint.pl/kurs/google-cloud-kurs-video-zostan-administratorem-systemow-it-piotr-tenyszyn,vgoclo.htm)


#### **Konteneryzacja**
Konteneryzacja ułatwia konfiguracje czy uruchamianie aplikacji w twoim środowisku. Przenoszenie konfiguracji ze środwiska testowego na produkcyjne nigdy nie było takie łatwe.  

1. Kurs z tematyki konteneryzacji na silniku [Docker](https://videopoint.pl/kurs/docker-kurs-video-zostan-administratorem-systemow-it-piotr-tenyszyn,vdockv.htm)

2. Kurs [docker dla zaawansowanych](https://strefakursow.pl/kursy/programowanie/kurs_docker_dla_zaawansowanych.html?ref=52966)

3. Kubernetes jest orchiestratorem który spina ze sobą platformę konteneryzacji w jeden wspolny klaster - [Kurs podstaw kubernetes(k8s)](https://strefakursow.pl/kursy/it_i_oprogramowanie/kurs_kubernetes_od_podstaw_-_zarzadzanie_i_automatyzacja_kontenerow.html?ref=52966)

4. Kurs docker z praktycznym i przykładami - [Docker dla developera](https://strefakursow.pl/kursy/programowanie/kurs_docker_-_srodowiska_developerskie.html?ref=52966)

#### **Programowanie**
Tu znajdziesz kursy programowania w języku python i go. Tu znajdziesz pełną ścieżkę [python developera](https://strefakursow.pl/sciezki_kariery/python_developer.html?ref=52966)

1. Podstawy języka [programowania w GO](https://strefakursow.pl/kursy/programowanie/fundamenty_programowania_w_jezyku_go.html?ref=52966)

2. Programowanie w [Python - zaawansowany](https://strefakursow.pl/kursy/programowanie/kurs_python_-_zaawansowany.html?ref=52966)
3. Programowanie w [Python - średniozaawansowany](https://strefakursow.pl/kursy/programowanie/kurs_python_-_sredniozaawansowany.html?ref=52966)

#### **System Kontroli Wersji**
Git - najpopularniejszy system kontroli wersji - poznaj jego podstawy i zacznij korzystać.

1. Kurs [Git dla zaawansowanych](https://strefakursow.pl/kursy/programowanie/kurs_git_dla_zaawansowanych.html?ref=52966)
2. Kurs [Git dla poczatkujących](https://strefakursow.pl/kursy/programowanie/kurs_git_dla_poczatkujacych.html?ref=52966)


### Dla Administratora Systemów / Support IT
Systemy linux i jego narzędzia są szeroko wykorzystywane w świecie biznesoym przez korporacje z dziedziny IT - oto kilka kursów i ścieżek kariery.
1. Kurs dla [Administratora Systemu Linux](https://strefakursow.pl/sciezki_kariery/administrator_linux.html?ref=52966)

2. Kurs związany z [bezpieczeństwem środwisk IT](https://strefakursow.pl/sciezki_kariery/ekspert_ds_cyberbezpieczenstwa.html?ref=52966)

3. Administracja systemem [Linux Ubintu 20.04](https://videopoint.pl/kurs/ubuntu-20-04-kurs-video-zostan-administratorem-systemow-it-piotr-koska,vubun2.htm)

4. Wprowadzenie do tematu [Ethical Hacking](https://strefakursow.pl/kursy/it_i_oprogramowanie/kurs_ethical_hacking_i_cyberbezpieczenstwo_od_podstaw.html?ref=52966)

5. Zagadnienia [Sieciowe w systemie Linux](https://strefakursow.pl/kursy/it_i_oprogramowanie/kurs_podstawy_networkingu_oraz_konfiguracji_sieci_w_linux.html?ref=52966)

6. Administracja systemem [Linux](https://strefakursow.pl/kursy/it_i_oprogramowanie/administracja_serwerem_linux.html?ref=52966)

7. Administracja systemem [Linux dla Zaawansowanych](https://strefakursow.pl/kursy/it_i_oprogramowanie/zaawansowana_administracja_systemem_linux.html?ref=52966)

8. System [Linux dla początkujących](https://strefakursow.pl/kursy/it_i_oprogramowanie/kurs_linux_dla_poczatkujacych.html?ref=52966)

#### **Powłoki systemowe i narzędzia**

1. [Bash Podstawy](https://videopoint.pl/kurs/bash-kurs-video-zostan-administratorem-systemow-it-piotr-koska-piotr-tenyszyn,vbashv.htm)

2. [Bash dla zaawansowanych](https://videopoint.pl/kurs/bash-techniki-zaawansowane-kurs-video-zostan-administratorem-systemow-it-piotr-koska,vbashz.htm)

3. [Poznaj Edytor Vim](https://videopoint.pl/kurs/vim-kurs-video-zostan-administratorem-systemow-it-piotr-koska-piotr-tenyszyn,vvimv.htm)

6. [Jak pracować z Nginx](https://strefakursow.pl/kursy/it_i_oprogramowanie/kurs_nginx_-_wydajne_serwery_od_podstaw.html?ref=52966)

7. [Techniki pracy z Vim](https://strefakursow.pl/kursy/programowanie/kurs_vim_-_techniki_pracy.html?ref=52966)

8. Automatyzacja i skrypty w [bash](https://strefakursow.pl/kursy/programowanie/kurs_bash_-_skrypty_i_automatyzacja.html?ref=52966)

#### **Bazy danych**
Ścieżka karier któa pozwoli zrozumieć Ci zagadnienia związane z [bazami danych](https://strefakursow.pl/sciezki_kariery/administrator_baz_danych.html)

1. Kurs obsługi i zarządznia bazą danych [PostgresSQL](https://videopoint.pl/kurs/postgresql-kurs-video-zostan-administratorem-systemow-it-piotr-tenyszyn,vpostz.htm)

2. Praktyczny [kurs obsługi bazy danych](https://strefakursow.pl/kursy/programowanie/kurs_postgresql_-_administracja_bazami_danych.html?ref=52966)
