package main

import "fmt"

func main() {
	var anInteger int = 42
	anInteger := 42

	const anInteger int = 42
	const anInteger = 42
}