package main

import (
	"fmt"
	"io"
	"os"
	"io/ioutil"
)

func main() {

	content := "Hello from Go!"

	file, err := os.Create("./exemple.txt")
	checkError(err)
	defer file.Close()

	ln, err := io.WriteString(file, content)
	checkError(err)

	fmt.Println("All done with file of %v characters", ln)

	bytes := []byte(content)
	ioutil.WriteFile("./byte_example.txt", bytes, 0644)

}

func checkError(err error) {
	if err !=nil {
		panic(err)
	}
}