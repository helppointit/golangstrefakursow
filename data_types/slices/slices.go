package main

import "fmt"

func main() {
	names := make([]string, 4)
	//names = append(names, "Piotr")
	///names = append(names, "Monika", "Milosz")
	names = append(names, "5 element")
	names[0] = "Piotr"
	names[1] = "Monika"
	names[2] = "Milosz"
	names[3] = "Emila"
	
	fmt.Println(names)
	fmt.Println(names[4])
}